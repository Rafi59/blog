---
cache_enable: false
visible: false
---

[g-navbar id="navbar1" name=navbar1 fixed=top centering=none brand_text="Blog de Rafi59" render=false]
    [g-navbar-menu name=menu0 alignment="center" submenu="autre" attributes="class:highdensity-menu"][/g-navbar-menu]    
    [g-navbar-menu name=menu1 icon_type="fontawesome" alignment="right" ]
        [g-link url="https://framasphere.org/u/rafi59" icon_type="fontawesome" icon="asterisk"][/g-link]
        [g-link url="https://framapiaf.org/@Rafi59" icon="retweet"][/g-link]
        [g-link url="https://framagit.org/Rafi564/framasite" icon="git"][/g-link]
        [g-link url="https://github.com/Rafi564" icon="github"][/g-link]
    [/g-navbar-menu]
[/g-navbar]
<script type="text/javascript" src="https://sense.framasoft.org/sense3.js" data-sense3="728x90;degooglisons,firefox,framabookin,linux,mozilla,twitter,ubuntu"></script>
<img src="https://framaclic.org/h/blog-rafi59" alt="">
[g-footer-one name="footer" render=false]
[g-section name="credits"]
<script type="text/javascript" src="https://sense.framasoft.org/sense3.js" data-sense3="728x90;"></script>
Hébergé par [Framasoft](https://framasoft.org/)
Ce site est motorisé par [Grav CMS](http://getgrav.org/) avec le theme [Gravstrap](http://diblas.net/themes/gravstrap-theme-to-start-grav-cms-site-with-bootstrap-support/) adapté par [Framasoft](https://framasoft.org/)

[/g-section]
[/g-footer-one]