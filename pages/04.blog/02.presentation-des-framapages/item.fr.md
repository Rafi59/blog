---
title: 'Présentation des Framapages'
media_order: 'framapages1.png,framapages2.png,framapages3.png,framapages4.png'
date: '01-04-2018 12:56'
taxonomy:
    category:
        - blog
        - framasoft
        - framapages
        - noemiecms
    tag:
        - Framasoft
        - Framasite
        - Framapages
        - NoémieCMS
---

---
title: 'Présentation des Framapages'
media_order: 'framapages1.png,framapages2.png,framapages3.png,framapages4.png'
date: '01-04-2018 12:56'
taxonomy:
    category:
        - blog
        - framasoft
        - framapages
        - noemiecms
    tag:
        - Framasoft
        - Framasite
        - Framapages
        - NoémieCMS
---

Dans cet article je vais présenter les Framapages.

Les Framapages sont un service proposé par l'association Framasoft et qui permet de créer une page web très facilement. Elles sont propulsées par <a href="https://framagit.org/framasoft/PrettyNoemieCMS">NoémieCMS</a>


## Créer une page
On commence donc par créer un compte sur <a href="https://frama.site/">https://frama.site/</a> et on clique sur nouveau site, ensuite ce menu s'affiche et on chosit Créer une page.


![](framapages1.png) {.img-responsive}

Remplissez le formulaire

![](framapages2.png) {.img-responsive}

Dès que vous avez fini de le remplir faites Créer la page
## La modification
Maintenant que nous avons créé notre page nous allons la remplir.

Allez à l'url https://lenomquevousavezdonnedansleformulaire.frama.site/login

Remplissez les champs Login et Mot de Passe

![](framapages3.png) {.img-responsive}

Et voici votre site !

Voici la liste des modules disponibles

![](framapages4.png) {.img-responsive}

## Petite vidéo
<iframe src="https://peertube.cpy.re/videos/embed/1174e225-1a2e-4e6c-8ab4-1b709cd46978" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Je ferai un autre article avec la présentation des modules.

À bientôt